import RNFetchBlob from 'rn-fetch-blob';

const downloadSizeInBits = 12000000;
const metric = 'MBps';
export const measureConnectionSpeed = (): any => {

const imageURI= 'https://drive.google.com/file/d/1QNimYlx8WHqTh5nI7g7Dc32yH8w-fecX/view?usp=sharing';

  return new Promise((resolve, reject) => {
    const startTime = (new Date()).getTime();
    RNFetchBlob
      .config({
        fileCache: false,
      })
      .fetch('GET', imageURI, {})
      .then((res) => {
        const endTime = (new Date()).getTime();
        const duration = (endTime - startTime)/ 1000;
        const speed = (downloadSizeInBits/ (1024 * 1024 * duration));

        resolve({metric, speed});
      })
      .catch(reject);
  });
};