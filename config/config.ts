export const config = {
    apiRootPath: "https://expleoconnect.azurewebsites.net/",
    authentication: "v1/authenticate",
    requestCallbacks:"v1/requestCallbacks?userId=",
    userInterests:"v1/userInterests/",
    updateInterests:"v1/userInterests",
    register:"v2/register",
    requestCallback:"v1/requestCallback",
};