import React, { Component } from 'react'
import { Text, Picker, View, StyleSheet, Dimensions, Image, Keyboard, KeyboardAvoidingView, Alert, TouchableOpacity } from 'react-native'
import Animated, { Easing, Value } from 'react-native-reanimated'
import styles from './styles'
import { connect } from 'react-redux'
import { clearData } from '../actions/auth.action'

type MyProps = {
    navigation: any,
    authData: any,
    clearData: () => void
}
type MyState = {
    isReady: boolean,
    textOpacity: any,
    selectedCountry: any,
    settings: any
}
const { width, height } = Dimensions.get('window');
class LeftMenu extends React.Component<MyProps, MyState> {

    navigation: any;
    constructor(props: any) {
        super(props);
        this.navigation = props['navigation'];
        this.state = {
            isReady: false,
            textOpacity: new Value(1),
            selectedCountry: '',
            settings: false
        }
    }

    setSelectedCountry(value: any) {
        this.setState({ selectedCountry: value });
    }

    static navigationOptions = ({ navigation }: { navigation: any }) => {
        return {
            //Heading/title of the header
            title: navigation.getParam('Title', 'Left Right Custom Header'),
            //Heading style
            headerStyle: {
                backgroundColor: navigation.getParam('BackgroundColor', 'red'),
            },
            //Heading text color
            headerTintColor: navigation.getParam('HeaderTintColor', '#fff'),
            headerRight: (
                <TouchableOpacity onPress={() => Alert.alert('Right Menu Clicked')}>
                    <Text
                        style={{
                            color: 'white',
                        }}>
                        Right Menu
              </Text>
                </TouchableOpacity>
            ),
            headerLeft: (
                <TouchableOpacity onPress={() => navigation.navigate('FirstPage')}>
                    <Text
                        style={{
                            color: 'white',
                        }}>
                        Left Menu
              </Text>
                </TouchableOpacity>
            ),
        };
    };

    render() {
        return (
            <View>
                <View>
                    <Image
                        source={require('../../assets/bg1.png')}
                        style={{ width: undefined, height: 250 }}
                    >
                    </Image>
                    <Image
                        source={require('../../assets/user.png')}
                        style={{ marginLeft: 110, backgroundColor: 'white', marginRight: 110, marginTop: -225, width: 200, height: 200, borderColor: 'white', borderWidth: 1, borderRadius: 200 }}
                    ></Image>
                </View>
                <View style={{ backgroundColor: 'black', height: '100%' }}>
                    <View style={{ marginTop: 20, marginLeft: 'auto', marginRight: 'auto' }}>
                        <Text style={styles.profileText}>Welcome, <Text style={{ fontWeight: 'bold' }}>{this.props.authData.firstName} {this.props.authData.lastName} {this.props.authData.isApproved === 'Y' ? (<Image
                            source={require('../../assets/tick.png')}
                            style={{ width: 25, height: 20 }}
                        />) : (null
                        )}</Text></Text>
                    </View>
                    <View style={{ margin: 20 }}>
                        <TouchableOpacity onPress={() => {
                            this.navigation.navigate('MyRequests')
                        }}>
                            <View style={containerStyle.rowContainer}>
                                <Image
                                    source={require('../../assets/Database.png')}
                                    style={{  width: 28, height: 40 }}
                                />
                                <Text style={styles.profileTextKey}>  My Requests</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ margin: 20 }}>
                        <TouchableOpacity onPress={() => {
                            let settings = this.state.settings;
                            this.setState({ settings: !settings });
                        }}>
                            <View style={containerStyle.rowContainer}>
                                <Image
                                    source={require('../../assets/Settings.png')}
                                    style={{ width: 40, height: 40 }}
                                />
                                <Text style={styles.profileTextKey}>  Settings</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    {this.state.settings ? (<View style={{ marginVertical: 10, marginLeft: 60 }}>
                        <TouchableOpacity onPress={() => {
                            this.navigation.navigate('Interests')
                        }}>
                            <View style={containerStyle.rowContainer}>
                                <Image
                                    source={require('../../assets/Interest.png')}
                                    style={{ width: 35, height: 35 }}
                                />
                                <Text style={[styles.profileTextKey]}>  Interests</Text>
                            </View>
                        </TouchableOpacity>
                    </View>) : (undefined)}
                    <View style={{ margin: 20 }}>
                        <TouchableOpacity onPress={() => {
                            this.props.clearData();
                            this.navigation.navigate('Login')
                        }}>
                            <View style={containerStyle.rowContainer}>
                                <Image
                                    source={require('../../assets/logout.png')}
                                    style={{ width: 35, height: 35 }}
                                />
                                <Text style={[styles.profileTextKey]}>  Logout</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const containerStyle = StyleSheet.create({
    container: {
        padding: 8,
        backgroundColor: "#ffffff",
    },
    rowContainer: {
        flexDirection: 'row'
    }
});

const mapStateToProps = (state: any) => ({
    authData: state.authData
});

function bindToAction(dispatch: any) {
    return {
        clearData: () => dispatch(clearData())
    };
}

export default connect(mapStateToProps, bindToAction)(LeftMenu);